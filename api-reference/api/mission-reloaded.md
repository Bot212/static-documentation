# [VTOLAPI](/documentation/api/index.html).MissionReloaded

public static UnityAction **MissionReloaded**;

### Description

MissionReloaded gets invoked whenever the user is in a mission and reloads it from a quicksave or restarts it from death.

```cs
public override void ModLoaded()
{
    VTOLAPI.MissionReloaded += MissionReloaded;
    base.ModLoaded();
}

private void MissionReloaded()
{
    //My Amazing Code to run when the user reloads the mission
}
```