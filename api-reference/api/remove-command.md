# [VTOLAPI](/documentation/api/index.html).RemoveCommand

## Declaration
public void **RemoveCommand**(string **command**);

## Parameters
 command | The command prefix to remove
-|-

## Description
Removes a command from the mod loader console.

```cs
public GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);

public override void ModLoaded()
{
    VTOLAPI.instance.CreateCommand("command", commandCallBack); //this is not a static method so you must create the command on the instance of the VTOLAPI
    base.ModLoaded();
}

private void commandCallBack(string command)
{
    if (command.Equals("command active")) //Make sure to include the command prefix here
    {
        cube.SetActive(true);
        cube.transform.position = new Vector3(0, 0, 0);
        Debug.Log("Cube set to active and moved");
    }
    else Debug.Log("Command called but unknown");
}
```