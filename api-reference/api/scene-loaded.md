# [VTOLAPI](/documentation/api/index.html).SceneLoaded

public static UnityEvent<VTOLScenes> **SceneLoaded**;

## Description

Run code when the scene has finished loading. 

```cs
public override void ModLoaded()
{
    VTOLAPI.SceneLoaded += SceneLoaded;
    base.ModLoaded();
}

private void SceneLoaded(VTOLScenes scene)
{
    switch (scene)
    {
        case VTOLScenes.ReadyRoom:
            break;
        case VTOLScenes.Akutan:
        case VTOLScenes.CustomMapBase:
            Log("Map Loaded");
            break;
        case VTOLScenes.LoadingScene:
            break;
    }
}
```

If you want to wait for a flight scene, consider using this version of `SceneLoaded`

```cs
private void SceneLoaded(VTOLScenes scene) 
{
    if (scene == VTOLScenes.Akutan || scene == VTOLScenes.CustomMapBase || scene == VTOLScenes.CustomMapBase_OverCloud) // If inside of a scene that you can fly in
    {
        Debug.Log("A flight scene has loaded");
    }
}```
