# VTOLAPI

This is the documentation for the mod loaders API

## Static Properties
Name | Description
-|-
instance | The current instance of the API in the game world
[currentScene](/documentation/api/current-scene.html) | The scene that is currently loaded

## Methods
Name | Description
-|-
[CreateCommand](/documentation/api/create-command.html) | Adds a command to the mod loader console
[RemoveCommand](/documentation/api/remove-command.html) | Removes a command from the mod loader console

## Static Methods
Name| Description
-|-
[CreateSettingsMenu](/documentation/api/create-settings-menu.html) | Creates a page in the mod settings tab
[GetPlayersVehicleGameObject](/documentation/api/get-players-vehicle-gameobject.html) | Gets the GameObject of the player's vehicle
[GetUsersMods](/documentation/api/get-users-mods.html) | Gets a list of the mods that are currently loaded
[SteamID](/documentation/api/steam-id.html) | Returns the player's Steam ID
[SteamName](/documentation/api/steam-name.html) | Returns the player's Steam name

## Events
Name | Description
-|-
[SceneLoaded](/documentation/api/.html) | Creates a 
[MissionReloaded](/documentation/api/mission-reloaded.html) | Returns the GameObject of the player's vehicle