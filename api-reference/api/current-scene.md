# [VTOLAPI](/documentation/api/index.html).currentScene

public static VTOLScenes **currentScene**;

## Description

If for whatever reason you needed to retrieve the currently loaded scene, `currentScene` stores it.

```cs
public bool isSceneAkutan() {
    if (VTOLAPI.currentScene == VTOLScenes.Akutan) return true;
    else return false;
}
```