# [VTOLAPI](/documentation/api/index.html).GetPlayersVehicleGameObject

## Declaration
public static GameObject **GetPlayersVehicleGameObject**();

## Parameters
 command | The command prefix to activate the callBack 
-|-
**callBack** | **The method to call on receiving the command prefix in the console** 

This returns the game object of the vehicle which the player is flying if the vehicle was not found, it will just return null so make sure to have a null check! 
```cs
GameObject currentVehicle = VTOLAPI.GetPlayersVehicleGameObject();
```