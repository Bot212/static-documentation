# [VTOLAPI](/documentation/api/index.html).CreateSettingsMenu(Settings settings)

## Description
Creates a settings page in the `mod settings` tab. Make sure to fully create your settings before calling this as you can't change it onces it's created.

## Declaration
public static void **CreateSettingsMenu**(Settings **newSettings**) 

## Parameters
newSettings | Parameters for making your settings menu
-|-

## Example
```cs
private static Settings _setting;
private static UnityAction<float> _amountChanged;
private static float _gravityAmount = 0;


public override void ModLoaded()
{
    base.ModLoaded();
    _amountChanged += ChangedValue;

    _setting = new Settings(this);
    _setting.CreateCustomLabel("This will change the amount of gravity when turned on");
    _setting.CreateCustomLabel("Default = -9.3");
    _setting.CreateFloatSetting("Toggled Amount", _amountChanged, _gravityAmount);
    VTOLAPI.CreateSettingsMenu(_setting);
}

public void ChangedValue(float amount)
{
    _gravityAmount = amount;
}
```

## Existing Mod Examples
- [Air Traffic Mod](https://github.com/THE-GREAT-OVERLORD-OF-ALL-CHEESE/AirTraffic/blob/7f7232159b63b888d32c54e3d2660d10727f0405/AirTraffic/Main.cs#L119)

> [!NOTE]
> This page is not yet completed, for more inforation on the settings menu please see [this](https://gitlab.com/vtolvr-mods/ModLoader/-/blob/release/ModLoader/SettingsMenu.cs)