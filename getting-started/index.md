# Requirements
There are a few requirements to start using the VTOL VR Mod Loader.
## Steam Version of VTOL VR.
The Mod Loader only works for the Steam version of VTOL VR, not any other version on any other platform.
You can find the Steam version [here](https://store.steampowered.com/app/667970/VTOL_VR/).

# Downloading
You can now download the VTOL VR Mod Loader from our [home page](https://vtolvr-mods.com/).

![Image of the home page with mouse over download button](/images/getting-started/download_button.png)

Once it is downloaded, place this file in a location you would use such as your Desktop.

>[!NOTE]
> As of version 5.0.0, you no longer need to run the application every time you  want to play with mods. You only need to run it when you want to check for updates to the Mod Loader, mods and skins.

For the installation process, you will need to run it as an administrator.

![Image of admin popup](/images/getting-started/admin_popup.png)

>[!WARNING]
> If you do not have admin privileges on your PC, this is fine. You can run it and install it without admin privileges however, **one-click install will not work**, you will have to download the zip files and place them into the correct folder yourself.

The Mod Loader needs to know where your copy of VTOL VR is installed to. The "Auto Detect" button should be able to find it using common steam files, however, if this isn't the case you can manually press "Browse" and select the `VTOLVR.exe`.

![Image of installation screen](/images/getting-started/auto_detect.png)

Now you can press install, once all the downloads are complete in the downloads tab you can play with mods.

If you need any more support, feel free to join our [discord](https://discord.gg/XZeeafp).