> [!NOTE]
> This issue should be fixed in update 5.0.4, but this page is here incase people still run into it.


# To run this application, you must install .NET

![.net Popup](/images/getting-started/dotnet_popup.png)

From version 5.0.0 to 5.0.3 many users who did not have .NET core 5.x runtime installed would get this popup when launching the Mod Loader.

## How to install .NET 5.x Runtime

To download .NET 5.0 go over to this [link](https://dotnet.microsoft.com/download/dotnet/5.0/runtime?utm_source=getdotnetcore&utm_medium=referral) and press "Download x64" under "Run desktop apps".

![Image of download page for .net 5.0](/images/getting-started/download_dotnet.png)