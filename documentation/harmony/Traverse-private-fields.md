# Traverse
Traverse is the class in Harmony that is used to get private variables or functions, a traverse is created as follows.
```cs
Traverse traverse = Traverse.Create(object: objectToTraverse);
```
After making your traverse, you can read private fields by calling `Traverse.Field("yourField").GetValue()`, and you can call private functions by doing `Traverse.Method("yourmethod").GetValue()`, note the return on get value is of type object so you will need to cast it correctly.  
