When you have the campaign, you can get a list of all the scenarios inside that campaign.
```cs
VTResources.GetBuiltInCampaign("campaignID").allScenarios; 
VTResources.GetCustomCampaign("campaignID").allScenarios;
//Returns a list of the class 'VTScenarioInfo'
```