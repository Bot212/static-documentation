You can send messages to the flight logger by using the class FlightLogger. This can be useful when you want to debug values to see inside of VR without taking off the headset.
```cs
FlightLogger.Log("My Message");
```