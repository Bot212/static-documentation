Inside the games class TargetManager, there is a list of all the current actors in the scene. Each actor registers and unregisters with this list when they get spawned and destroyed so make sure to keep checking this list in case the one you are using has been destroyed. There also is allied and enemy lists in the class TargetManager if you just want one team.
```cs
TargetManager.instance.allActors;
TargetManager.instance.alliedUnits;
TargetManager.instance.enemyUnits;
TargetManager.instance.detectedByAllies;
TargetManager.instance.detectedByEnemies;
//All return a list of the class actor.
```