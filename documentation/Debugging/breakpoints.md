# Breakpoints in dnSpy

> [!NOTE]
> Ensure that the Mod Loader installed is version 5.4.0 or higher.

With dnSpy, you can set breakpoints in the game code and mod code of VTOL VR. To enable this functionality, you need to modify Doorstop's configuration file (`doorstop_config.ini`) to activate the Mono debugger server.

# How to connect

1. Navigate to the root folder of your game and locate the Doorstop configuration file (`doorstop_config.ini`).
1. Open the configuration file and locate line 30.
1. Change the value of `debug_enabled` to `true`.
1. Launch VTOL VR
1. Open dnSpy
1. Open the dlls you wish to add breakpoints to `File > Open...`

![File menu opened and mouse over "Open..."](/images/documentation/Debugging/dnspy_opening_dll.png)

7. Start Debugging `Debug > Start Debugging...`

![Debug menu open with mouse over "Start Debugging..."](/images/documentation/Debugging/dnspy_start_debugging.png)

8. Select `Unity (Connect)` on the dropdown next to `Debug engine`
1. Set the IP Address to `127.0.0.1`
1. Set the port to `10000`
1. Press `Ok`

![Image of the values been set in the new window](/images/documentation/Debugging/dnspy_debug_program.png)

# Verifying it connected

Now that you've completed the steps to connect dnSpy to VTOL VR through the Mono debugger server, you can verify that it's working correctly. 

Check for a solid red circle on your breakpoints - this indicates that the debugger is successfully attached and you can start setting breakpoints in both the game code and your mod code.

![Image of breakpoint with modules loaded. Showing a solid red circle](/images/documentation/Debugging/dnSpy_breakpoint_example.png)