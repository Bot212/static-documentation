>[!WARNING]
> Sorry, this guide isn't fully completed yet. There are so many different methods to creating a skin.
>
> Feel free to ask any questions in the modding discord. 


> [!WARNING]
> This guide is using free software, Blender. But there are tons of others out there, so if you know about 3D modeling and texturing feel free to use your software of choice. 
>
> At the end, all you need to be able to do is export the textures as a png file

# Getting the Models and Textures

The program required to get the models and textures is Asset Studio.

You can download the latest release from [here](https://github.com/Perfare/AssetStudio/releases)

For this guide we are going to be extracting the AV-42C, but the names and steps will be the same for any other objects.

First need to open the file `resources.assets` which is located in `VTOLVR > VTOLVR_Data > resources.assets`.

![Loading a file in Asset Studio](/images/creating-a-skin/asset-studio-opening-file.png)

Once it's finished opening, you'll want to search for the name of the vehicle you want to create a skin. It may take a few presses of the Enter button to find it. 

> VTOL4 = AV-42C
>
> FA-26B = F/A-26B
>
> SEVTF = F-45A

![Finding the prefab](/images/creating-a-skin/exporting-objects-merge.png)

When you have found it, select the checkbox and press `Model > Export selected objects (merge)`. This will export it as an fbx and the textures used by that model.

I would recommend exporting this into a new folder as it creates all the texture files as well. (Example of VTOL4 exported below) 

![Exported files](/images/creating-a-skin/example-exported.png)

# Using these exported files

Now we have the textures and the model. The next steps can vary massively.

The overall goal is to paint on these texture files to create the skin. The model file is just used for previewing without having to launch up the game.

> [!CAUTION]
> **Do not upload the model with your skin**. It will add pointless data and won't be approved. 

> [!INFO]
> If you know of another program/process of creating a skin and want to write it up on this page. Feel free to ask . Marsh.Mello . about forking the project and adding it in.

