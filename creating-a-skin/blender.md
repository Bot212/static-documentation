# Blender
This page will explain how to use the extracted models and textures in Blender.

To import the model go to `File > Import > FBX`, the final result will look similar to this.

There is a downside to this export process. As we are exporting it from a Unity prefab there is a lot of junk which we don't need such as the user interface on the plane and empty game objects. You can delete these to give you a clear view of the model or leave them in and just zoom in, don't worry if you break the model, you won't be using this for publishing your skin and you can just reimport the fbx.

Once you're happy with it, I would recommend saving the blender file so every time you don't have to keep deleting the junk off it. 

![All the extra junk](/images/creating-a-skin/result-in-blender.png)