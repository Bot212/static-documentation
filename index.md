# Welcome to the VTOL VR Mods Documentation!

## [Back to main site](https://vtolvr-mods.com/)

Here you can find information on how to use the Mod Loader and modding in VTOL VR.

If you would like to contribute to this documentation it is open source. You can find the repository [here](https://gitlab.com/vtolvr-mods/static-documentation)

## Useful links
- [Home Page](https://vtolvr-mods.com/)

- [Creating a mod](/creating-a-mod/index.html)

- [Creating a skin](/creating-a-skin/index.html)

- [Other documentation](/documentation/api/index.html)

- [dnSpy](https://github.com/dnSpy/dnSpy/releases/tag/v6.1.8)

dnSpy is used to view the games code.
- [Asset Studio](https://github.com/Perfare/AssetStudio)

Asset Studio is used for exploring and extracting game assets
- [Harmony Docs](https://github.com/pardeike/Harmony/wiki)

Harmony is used for getting private variables, running code before in-game methods, and much more.