# Getting Started
Let's begin by opening the mod loader and creating a new project named `Media Controls`.
Next, click on "Open Project" in the mod loader. You should see Visual Studio open to a blank screen.

## Visual Studio
### Adding a new class
On the left you have your "Solution Explorer". This is a window that contains all of the files in your project. To start, lets setup the files that we will need later. Click the dropdown arrow to the left of "MediaControls" in the solution explorer. Now right click on the box labeled "MediaControls", select `Add` and then `New Item...`.

Here is a list of tools that you can add to your project, but for our purposes we are just going to add a new class. 

![](/images/creating-a-mod/visual-studio-create-class.gif)

After creating the class, go to the Solution Explorer and rename `Class1.cs` to `HarmonyPatches.cs`. Click yes when prompted to change references.

We will come back to this later, but now you know how to use the Solution Explorer and create a file.

### Adding a reference
Another thing that we will have to setup before starting is our reference to Harmony. Harmony won't be used until the next section of this guide, but we will get it out of the way now.
Bring your attention to the solution explorer again. Under `MediaControls` you will find a tab named `References`. Right-click on `References` then press `Add Reference...`. Click `Browse` and navigate to your VTOL VR install folder (usually C:\Program Files (x86)\Steam\steamapps\common\VTOL VR), then `VTOLVR_Data\Managed`. Select the file labeled `0Harmony.dll` then press OK.

![](/images/creating-a-mod/visual-studio-add-reference.gif)

## The Logic
I want to brielfy describe how our system will work before we get into creating it. This is a good practice when making any project, but especially when coding. We will be using the Windows user32 API to simulate keyboard presses. As the API is in C++, we will need to use `DllImport` from [`System.Runtime.InteropServices`](https://learn.microsoft.com/en-us/dotnet/api/system.runtime.interopservices?view=net-7.0) to call its methods. We also need to know the correct keys to control the media player, but I already have those for you in the next section. Finally, we will make a method that can be called whenever a media button is pressed in game that will trigger our simulated keys.

## Main.cs
Here is where we will put the main logic in your code. Double click `Main.cs` in your solution explorer to open the file. You can rename this file to `MediaControls.cs` if desired, but I will leave it as the default for this tutorial. Viewing the code, you should see the sample methods created for you by default. Take a moment to read the comments and understand what each function does. For now, we only need the `ModLoaded()` function, so you can highlight and delete lines 20-47. Also delete line 17 as we are no longer using the `SceneLoaded` event.

This mod won't need most of the libraries that are included. Let's head to the second line and delete all of the `using` statements except for `using System;`. Now, as mentioned earlier, import `System.Runtime.InteropServices`. To do this, make a new line and type `using System.Runtime.InteropServices;`. Finally, we will also need a library called `Harmony`. Try importing Harmony yourself. When finished, your first few lines should look like this.
```cs
using System;
using System.Runtime.InteropServices;
using Harmony;
```

Before we can call the keys to interact with the media player, we need to know what they are. Move down the file to the class definition, which should look something like `public class Main : VTOLMOD`. We can define the media keys by putting these lines inside the brackets of the class definition:
```cs
public const int KEYEVENTF_KEYUP = 0;
public const int KEYEVENTF_EXTENTEDKEY = 1;
public const int VK_MEDIA_NEXT_TRACK = 0xB0;// code to jump to next track
public const int VK_MEDIA_PLAY_PAUSE = 0xB3;// code to play or pause a song
public const int VK_MEDIA_PREV_TRACK = 0xB1;// code to jump to prev track
```
We also need the `user32` dll in order to send the key presses. Let's import it right under those variables using this: 
```cs
[DllImport("user32.dll")]
```
Then define the "key press" function directly underneath using this:
```cs
public static extern void keybd_event(byte virtualKey, byte scanCode, uint flags, IntPtr extraInfo);
```

Navigate below the function `ModLoaded()` and add a new function `MediaPress` with an integer "key" as an input. We also want to make this function public, so that other classes can access it, and static, so that it can be called without an instance. If you did it correct, it should look like this:
```cs
public static void MediaPress(int key) {}
```

Inside this new function we need to check which key was pressed. This can be done using either multiple if statements or a switch case. Personally, I prefer using a switch case, so let's implement one where `0` represents play/pause, `1` represents next track, and `2` represents previous track.
```cs
switch (key) 
{
    case 0:
        //Play/Pause
        break;
    case 1:
        //Next track
        break;
    case 2:
        //Previous track
        break;
}
```

To finish off the media part of this mod, we need to actually call the keys. See if you can do this by yourself, you'll want to plug the key code variable into `virtual key`, `0` for scan code, `KEYEVENTF_EXTENEDEDKEY` for the flags, and `IntPtr.Zero` for extra info. A correct function call should look like this:
```cs
keybd_event(VK_MEDIA_PLAY_PAUSE, 0, KEYEVENTF_EXTENTEDKEY, IntPtr.Zero);
```

Your finished `Main.cs` should look like this:
```cs
using System;
using System.Runtime.InteropServices;
using Harmony;

namespace MediaControls
{
  public class Main : VTOLMOD
  {
    public const int KEYEVENTF_EXTENTEDKEY = 1;
    public const int KEYEVENTF_KEYUP = 0;
    public const int VK_MEDIA_NEXT_TRACK = 0xB0;// code to jump to next track
    public const int VK_MEDIA_PLAY_PAUSE = 0xB3;// code to play or pause a song
    public const int VK_MEDIA_PREV_TRACK = 0xB1;// code to jump to prev track

    [DllImport("user32.dll")]
    public static extern void keybd_event(byte virtualKey, byte scanCode, uint flags, IntPtr extraInfo);
    // This method is run once, when the Mod Loader is done initialising this game object
    public override void ModLoaded()
    {
      base.ModLoaded();
    }

    public static void MediaPress(int key)
    {
      switch (key)
      {
        case 0:
          //Play/Pause
          keybd_event(VK_MEDIA_PLAY_PAUSE, 0, KEYEVENTF_EXTENTEDKEY, IntPtr.Zero);
          break;
        case 1:
          //Next track
          keybd_event(VK_MEDIA_NEXT_TRACK, 0, KEYEVENTF_EXTENTEDKEY, IntPtr.Zero);
          break;
        case 2:
          //Previous track
          keybd_event(VK_MEDIA_PREV_TRACK, 0, KEYEVENTF_EXTENTEDKEY, IntPtr.Zero);
          break;
      }
    }
  }
}
```

> [!NOTE]
> Thank you to https://ourcodeworld.com/articles/read/128/how-to-play-pause-music-or-go-to-next-and-previous-track-from-windows-using-c-valid-for-all-windows-music-players for the information