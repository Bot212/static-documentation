# Advancing

Making mods is usually not easy, but hopefully this guide will enable you to advance from that first project to your own!

## Essential tools
The first step to creating advanced mods is having the right tools. Sure, you can try using a screwdriver on a nail, but you would make a lot more progress by just using a hammer. Some of these were marked optional in the first tutorial, so lets recap what is needed.

Here's what you will need:

- Visual Studio
- Unity
- Asset Ripper/dnSpy

## Visual Studio
If you haven't installed Visual Studio yet, check out the [creating a mod tutorial](/creating-a-mod/first-mod/setup.md).

## Asset Ripper/dnSpy
Depending on your end goal, you may choose one of these programs over the other. Asset Ripper allows you to decompile the game back into a Unity project that you can open while dnSpy allows you to decompile and view just the game's code. If you plan on creating or using assets in your mod, I would recommend using Asset Ripper.

### Installing and Using Asset Ripper
Please refer to the community tutorial below for instructions on how to download and use asset ripper with VTOL VR and Unity.\
![](https://www.youtube.com/watch?v=HMv4bdnY_yI)

### Installing dnSpy
Download the [latest dnSpy release from their github](https://github.com/dnSpyEx/dnSpy/releases/latest), extract the zip, and then open `dnSpy.exe`.  Now, navigate to your VTOL VR install folder and find the folder `VTOLVR_Data` and then `Managed`. Finally, drag the file named `Assembly-CSharp.dll` into the assembly explorer on the left side of the dnSpy window. You now have access to the game code of VTOL VR! Many of the scripts will be located under the little dash rather than the named spaces.

## Unity
For the time being, the game uses Unity version 2020.3.30f1. To download it, head to the [archive downloads on Unity's website](https://unity.com/releases/editor/archive), click on the 2020.X tab, and then scroll down to 2020.3.30.

Now you are ready to move on to the next section where we will recreate the "Media Controls" mod by the creator of the mod loader, .Marsh.Mello.

> [!NOTE]
> This tutorial is currently under development, for the time being please join our discord if you have any questions