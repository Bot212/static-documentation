# Implementing Harmony
We can't implement a Harmony patch if we don't even know what class or function we need to modify, so...

## What am I Patching?
To find the method that we need to patch, I will be using dnSpy. If you are using Unity, however, you can either look through the scripts folder or attempt to find the game object that is using the script.

### Getting Started with dnSpy
When you open dnSpy.exe, it may look familiar. The interface is heavily based off of Visual Studio, but the solution explorer is replaced with the assembly explorer (as we will be looking through assemblies). 
Now we need to add the game's script asssembly to dnSpy. Once again, navigate to your VTOL VR install folder then `VTOLVR_Data\Managed`. Find the file `Assembly-CSharp.dll`. Click and drag the file to the dnSpy window and drop it in the assembly explorer. A new folder should show up titled `Assembly-CSharp`. Open it, then open `Assembly-CSharp.dll`, and finally the folder labeled `-`.

![](/images/creating-a-mod/dnSpy-Assembly-CSharp.png)

### Finding the Media Controller
You can look through all of these scripts now to try and find what you need, but I'll give you the name of the one we are using here. Scroll through the list until you find `CockpitRadio`. Look through the file and see if you can identify the three methods that we will need to patch. If you chose `PlayButton`, `NextSong`, and `PrevSong` then you would be correct.

## Writing our Patches
Move back to your Visual Studio window and open the `HarmonyPatches.cs` file we made earlier. We can delete lines 9-11 and the imports at the top. We need to add `using HarmonyLib;` to the top again and now we can start on the patches.

### Play/Pause
Try making a new class called `PlayPatch` and then writing a prefix that calls our `MediaPress` function with the code for play/pause. It's alright if you can't, I'll go over it here. To begin, we need to tell Harmony what we want to patch. Since we are patching the class `CockpitRadio` and function `PlayButton`, we need something along the lines of:
```cs
[HarmonyPatch(typeof(CockpitRadio), nameof(CockpitRadio.PlayButton))]
```
Next we will make the class `PlayPatch`:
```cs
[HarmonyPatch(typeof(CockpitRadio), nameof(CockpitRadio.PlayButton))]
public class PlayPatch
{
    //Call our code when the play/pause button is pressed
}
```
Then add a prefix function that will return false to make sure the cockpit radio won't play:
```cs
[HarmonyPatch(typeof(CockpitRadio), nameof(CockpitRadio.PlayButton))]
public class PlayPatch
{
    //Call our code when the play/pause button is pressed
    public static bool Prefix()
    {
        //Press the media key
        return false;
    }
}
```
Finally, call our function to press the media key:
```cs
[HarmonyPatch(typeof(CockpitRadio), nameof(CockpitRadio.PlayButton))]
public class PlayPatch
{
    //Call our code when the play/pause button is pressed
    public static bool Prefix()
    {
        //Press the media key
        Main.MediaPress(0); //0 is the code for play/pause
        return false;
    }
}
```

### Next/Previous
See if you can make the next two patches on your own, you should only have to slightly modify the first one to make it work. Here is what they should look like:
```cs
[HarmonyPatch(typeof(CockpitRadio), nameof(CockpitRadio.NextSong))]
public class NextPatch
{
    //Call our code when the play/pause button is pressed
    public static bool Prefix()
    {
        //Press the media key
        Main.MediaPress(1); //1 is the code for next song
        return false;
    }
}
[HarmonyPatch(typeof(CockpitRadio), nameof(CockpitRadio.PrevSong))]
public class PrevPatch
{
    //Call our code when the play/pause button is pressed
    public static bool Prefix()
    {
        //Press the media key
        Main.MediaPress(2); //2 is the code for previous song
        return false;
    }
}
```

All that is left is to tell Harmony to use the patches!

## Call the Patches
Head back to `Main.cs` and add `using System.Reflection;` to the top. Inside the `ModLoaded` function we need to add the two lines from earlier:
```cs
public override void ModLoaded()
{
    HarmonyInstance harmonyInstance = HarmonyInstance.Create("YourName.MediaControls");
    harmonyInstance.PatchAll(Assembly.GetExecutingAssembly());
    base.ModLoaded();
}
```

# Build the Mod
If you haven't already, copy `VTOLVR-Modloader.exe` into the VTOLVR_ModLoader folder in your VTOL install. Don't forget to save your files, then press Ctrl+Shift+B to build your project.

Congratulations, you should now have a working mod! If you want to add a volume knob that will adjust the system media player or see how you can make the mod slightly more advanced, [check out the original by Marsh](https://gitlab.com/ben-w/media-controls/-/tree/master/MediaControls).

***Have fun modding!***