# Harmony

## What is Harmony?
Harmony is a way to patch or modify C# applications at runtime. You can [read more about it here](https://github.com/pardeike/Harmony/wiki).
We will be using Harmony in order to insert our code before game code runs and then we can tell Harmony to not let the game's code finish. By doing this the media player will be activated, but the audio from the game will never be triggered.

## How do we use Harmony?
Harmony is very straightforward to use. Let's say we have an application with a class named `Test` and function named `activateSomething`.
```cs
public class Test 
{
    private AnotherClass something = new AnotherClass();
    public bool shouldRun = true;
    public void activateSomething() 
    {
        something.Activate();
    }
}
```
If we wanted to run our own method when `activateSomething()` is called, then we can use a Haromny patch. Harmony patches come in three flavors: Prefix (continue with the original function), Prefix (don't run the original function), and Postfix. In a prefix you can change the definition from `public static void Prefix()` to `public static bool Prefix()`. With the definition of prefix set to a boolean, returning false would tell Harmony to run our code but then don't run the original code. It could even look something like this:
```cs
[HarmonyPatch(typeof(Test), nameof(Test.activateSomething))]
public class testPatch
{
    public static bool Prefix(ref bool __shouldRun) 
    {
        if (__shouldRun)
        {
            ourMethod();
            __shouldRun = false;
            return false;
        } 
        else 
        {
            return true;
        }
    }
    public static void ourMethod() 
    {
        Debug.Log("Don't activate something");
    }
}
```
There are three key things in that code that I want to point out. At the top you should notice `[HarmonyPatch()]`. Whenever making a patch, you need to add that line above the class to tell Harmony what it is supposed to be patching. `HarmonyPatch` takes two arguments: the class as `typeof(ClassName)`, and the function inside the class as either `"FunctionName"` or `nameof(ClassName.FunctionName)`. Another key thing is the argument in Prefix `ref bool __shouldRun`. By using this argument we are able to get a reference to the variable in the original function `shouldRun` allowing us to read and modify it. Finally, the ability to change whether or not to run the original code by changing the return can also be useful for many mods.

Harmony won't automatically patch, however, so we need to tell it to. For use in a mod, we just need to add 3 lines. The first line is to add the `System.Reflection` class to our main file:
```cs
using System.Reflection;
```
And then we need to add two lines to the `ModLoaded` function:
```cs
HarmonyInstance harmonyInstance = HarmonyInstance.Create("YourName.YourMod");
harmonyInstance.PatchAll(Assembly.GetExecutingAssembly());
```
This tells Harmony to look through all of your classes and then implement all patches it can find.

In the next section we will use this knowledge to incorporate our mod into the game!

> [!NOTE]
> Currently the mod loader is using an old version of Harmony (1.2.0.1). More information on using this version of Harmony can be found in [the documentation section](/documentation/harmony/patching.html) or the [Harmony docs](https://github.com/pardeike/Harmony/wiki/Patching).