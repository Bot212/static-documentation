>[!NOTE]
> If you feel like this guide is missing something or has some incorrect information. Feel free to let us know by email or on the discord server.
> If you know how to fix the issue, feel free to improve these docs with a merge request [here](https://gitlab.com/vtolvr-mods/static-documentation/-/tree/Development)!

# Creating your first mod!
This text guide will show you the up to date method on how to create a basic mod for VTOL VR using the mod loader at vtolvr-mods.com.

In this guide, we will go through the process of installing the software you need, creating the project, writing the code and testing it in the game.

# Overall Process

Modding in VTOL VR isn't supported by the developer, so there are a few differences to native modding games. Mods for VTOL VR consist of a Class Library in the .NET Framework. The Mod Loader searches these Class Libraries for a class called `VTOLMOD`. Then once that is found, it will create a new GameObject with do not destroy on load set to it (so it persists between scenes) and adds your `VTOLMOD` class onto that then calls `ModLoaded()`.

# More Questions?

If you have anymore questions about the process of creating a mod . Here are some useful places to get answers.

## [VTOL VR Modding Discord](https://discord.gg/XZeeafp)

The modding Discord is a great place to get specific questions answered. You can chat with current mod creators and with the general community who like playing with mods. 

We are small though so don't feel ignored if you don't get a response right away.

## [Unity Documentation](https://docs.unity3d.com/Manual/index.html)

If you haven't guessed already, VTOL VR is created in Unity, the Unity Docs are well documented explaining what each function does and how to use it.

## [Harmony Documentation (1.2.0.1)](https://github.com/pardeike/Harmony/wiki)

If you want to tweak existing code like stopping a method in the game from running or fetching a private variable, Harmony has got you covered!

This is the documentation for Harmony 1.2.0.1 (the version we use) and it will explain the basics of using Harmony.

[Also, have a check out our page about Harmony.](/documentation/harmony/patching.html)