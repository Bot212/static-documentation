# VTOL VR Modding Docs

This is the repository for the documentation about VTOL VR Modding. If you are wanting to add or remove something to the documentation, this is the place to be.

If you would just to read it, head over to the website here > [docs.vtolvr-mods.com](https://docs.vtolvr-mods.com/)

# Local Development
This documentation is using [Docfx](https://dotnet.github.io/docfx/index.html) to generate a static site, and hosted with [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)

## Installation
1. Open CMD and install docfx with the command `dotnet tool update -g docfx`
    
    ![CMD window open with the command typed in](images-readme/installing-docfx.png)
1. Fork, Clone and switch over to the `Development` branch before continuing.


## Viewing it locally
Each time you make a change, you need to rebuild the site. 

If you installed Visual Studio Code, you should be able to press `Terminal > Run Build Task` to build and host the site at http://localhost:8080/

If you haven't installed Visual Studio Code, you just need to run `docfx docfx.json --serve` in the root directory.

# Browser Development
If you just want to make small changes, they can be done all with in your browser. You will just be unable to test if it builds okay, which is fine for rewording existing pages.

1. Fork this repository by pressing [this](https://gitlab.com/vtolvr-mods/static-documentation/-/forks/new)
1. Switch to the `Development Branch` 

    ![Selecting the drop down and clicking on the word development](images-readme/Switching-Branches.png)
1. Open the Web IDE 

    ![Pressing the Web IDE button](images-readme/Opening-Web-IDE.png)

Now you should be in `gitlab.com`'s broswer version of Visual Studio Code

![Gitlab's IDE](images-readme/gitlab-ide.png)

# Deployment

To update the live site at `docs.vtolvr-mods.com` you need to create a merge request back to the `development` branch. Once the merge request is approved and merged by one of the team, it will be merged into production branch on our repository.

# Useful Links
- [Docfx's Guide on their Markdown](https://dotnet.github.io/docfx/docs/markdown.html?tabs=linux%2Cdotnet)
- [Docfx's Guide on how to create new pages](https://dotnet.github.io/docfx/docs/table-of-contents.html)